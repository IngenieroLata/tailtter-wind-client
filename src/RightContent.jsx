import React from 'react';

const RightContent = () => (
  <div className="w-1/4 pl-4">
    <div className="bg-white p-3 mb-3">
      <div>
        <span className="text-lg font-bold">Who to follow</span>
        <span>&middot;</span>
        <span>
          <a href="" className="text-xs">
            Refresh
          </a>
        </span>
        <span>&middot;</span>
        <span>
          <a href="" className="text-xs">
            View All
          </a>
        </span>
      </div>
    </div>
  </div>
);

export default RightContent;
