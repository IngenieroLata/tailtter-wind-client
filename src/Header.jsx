import React from 'react';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faHome,
  faBolt,
  faBell,
  faEnvelope,
  faSearch,
} from '@fortawesome/free-solid-svg-icons';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

library.add(faHome, faBolt, faBell, faEnvelope, faSearch, faTwitter);

const NavLink = ({ icon, name }) => (
  <a href="" className="top-nav-item">
    <FontAwesomeIcon icon={icon} /> {name}
  </a>
);

const Header = () => (
  <div className="bg-white">
    <div className="container mx-auto flex items-center py-4">
      <nav className="w-2/5">
        <NavLink icon="home" name="Home" />
        <NavLink icon="bolt" name="Moments" />
        <NavLink icon="bell" name="Notifications" />
        <NavLink icon="envelope" name="Messages" />
      </nav>
      <div className="w-1/5 text-center">
        <a href="">
          <FontAwesomeIcon
            icon={['fab', 'twitter']}
            size="lg"
            className="text-blue"
          />
        </a>
      </div>
      <div className="w-2/5 flex justify-end">
        <div className="mr-4 relative">
          <input
            type="text"
            className="bg-grey-lighter h-8 px-4 py-2 text-xs w-48 rounded-full"
            placeholder="Search Twitter"
          />
          <span className="flex items-center absolute pin-r pin-y mr-3">
            <FontAwesomeIcon icon="search" className="text-grey" />
          </span>
        </div>
        <div className="mr-4">
          <a href="">
            <img
              src="/avatar.png"
              alt="avatar"
              className="h-8 w-8 rounded-full"
            />
          </a>
        </div>
        <div>
          <button className="btn-teal">Tweet</button>
        </div>
      </div>
    </div>
  </div>
);

export default Header;
