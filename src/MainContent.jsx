import React from 'react';
import LeftProfile from './LeftProfile';
import RightContent from './RightContent';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faLink,
  faCalendar,
  faUser,
  faImages,
  faThumbtack,
  faChevronDown,
  faComment,
  faRetweet,
  faHeart,
  faEnvelope,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

library.add(
  faLink,
  faCalendar,
  faUser,
  faImages,
  faThumbtack,
  faChevronDown,
  faComment,
  faRetweet,
  faHeart,
  faEnvelope
);

const Tweet = () => (
  <div className="flex border-b border-solid border-grey-light">
    <div className="w-1/8 text-right pl-3 pt-3">
      <div>
        <FontAwesomeIcon
          icon="thumbtack"
          size="sm"
          className="text-teal mr-2"
        />
      </div>
      <div>
        <img src="avatar.png" alt="avatar" className="rounded-full h-12 w-12" />
      </div>
    </div>
    <div className="w-7/8 p-3">
      <div className="text-xs text-grey-dark">Pinned Tweet</div>
      <div className="flex justify-between">
        <div>
          <span className="font-bold">
            <a href="" className="text-black">
              Le profile
            </a>
          </span>
          <span className="text-grey-dark">@le-profile</span>
          <span className="text-grey-dark">&middot;</span>
          <span className="text-grey-dark">15 Dec 2017</span>
        </div>
        <div>
          <a href="" className="text-grey-dark hover:text-teal">
            <FontAwesomeIcon icon="chevron-down" />
          </a>
        </div>
      </div>
      <div className="mb-4">
        <p className="mb-6">🎉 le profile did something</p>
        <p className="mb-6">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate,
          reiciendis!
        </p>
        <p className="mb-4">
          <a href="">Lorem, ipsum dolor.</a>
        </p>
        <p>
          <a href="">
            <img
              src="image.jpg"
              alt=""
              className="border border-solid border-grey-light rounded-sm"
            />
          </a>
        </p>
      </div>
      <div className="pb-2">
        <span className="mr-8">
          <a
            href=""
            className="text-grey-dark hover:no-underline hover:text-blue-light"
          >
            <FontAwesomeIcon icon="comment" size="lg" className="mr-2" />9
          </a>
        </span>
        <span className="mr-8">
          <a
            href=""
            className="text-grey-dark hover:no-underline hover:text-green"
          >
            <FontAwesomeIcon icon="retweet" size="lg" className="mr-2" />20
          </a>
        </span>
        <span className="mr-8">
          <a
            href=""
            className="text-grey-dark hover:no-underline hover:text-red"
          >
            <FontAwesomeIcon icon="heart" size="lg" className="mr-2" />235
          </a>
        </span>
        <span className="mr-8">
          <a
            href=""
            className="text-grey-dark hover:no-underline hover:text-teal"
          >
            <FontAwesomeIcon icon="envelope" size="lg" className="mr-2" />
          </a>
        </span>
      </div>
    </div>
  </div>
);

const MainContent = () => (
  <div className="w-1/2 bg-white mb-4">
    <div className="p-3 text-lg font-bold border-b border-solid border-grey-light">
      <a href="" className="text-black mr-6">
        Tweets
      </a>
      <a href="" className="mr-6">
        Tweets & Replies
      </a>
      <a href="">Media</a>
    </div>

    <Tweet />
    <Tweet />
    <Tweet />
  </div>
);

export default MainContent;
