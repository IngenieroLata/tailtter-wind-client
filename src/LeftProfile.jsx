import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const LeftProfile = () => (
  <div className="w-1/4 pr-6 mt-8 mb-4">
    <h1>
      <a href="" className="text-black">
        Le Profile
      </a>
    </h1>
    <div className="mb-4">
      <a href="" className="text-grey-darker">
        @le-profile
      </a>
    </div>
    <div className="mb-4">
      Lorem ipsum dolor sit amet consectetur adipisicing elit.{' '}
      <a href="">@Fugit</a> corrupti quasi animi, odio unde necessitatibus?
    </div>
    <div className="mb-2">
      <FontAwesomeIcon
        icon="link"
        size="lg"
        className="text-grey-darker mr-1"
      />
      <a href="">something goes here</a>
    </div>
    <div className="mb-2">
      <FontAwesomeIcon
        icon="calendar"
        size="lg"
        className="text-grey-darker mr-1"
      />
      <span>Joined August 2017</span>
    </div>
    <div className="mb-4">
      <button className="btn-teal w-full h-10">Tweet to le-profile</button>
    </div>
    <div className="mb-4">
      <FontAwesomeIcon
        icon="user"
        size="lg"
        className="text-grey-darker mr-1"
      />
      <a href="">27 followers you know</a>
    </div>
    <div className="mb-4">
      <a href="">
        <img
          src="/avatar.png"
          alt="avatar"
          className="w-12 h-12 rounded-full"
        />
        <img
          src="/avatar.png"
          alt="avatar"
          className="w-12 h-12 rounded-full"
        />
        <img
          src="/avatar.png"
          alt="avatar"
          className="w-12 h-12 rounded-full"
        />
        <img
          src="/avatar.png"
          alt="avatar"
          className="w-12 h-12 rounded-full"
        />
        <img
          src="/avatar.png"
          alt="avatar"
          className="w-12 h-12 rounded-full"
        />
        <img
          src="/avatar.png"
          alt="avatar"
          className="w-12 h-12 rounded-full"
        />
        <img
          src="/avatar.png"
          alt="avatar"
          className="w-12 h-12 rounded-full"
        />
        <img
          src="/avatar.png"
          alt="avatar"
          className="w-12 h-12 rounded-full"
        />
        <img
          src="/avatar.png"
          alt="avatar"
          className="w-12 h-12 rounded-full"
        />
        <img
          src="/avatar.png"
          alt="avatar"
          className="w-12 h-12 rounded-full"
        />
      </a>
    </div>
    <div className="mb-4">
      <FontAwesomeIcon
        icon="images"
        size="lg"
        className="text-grey-darker mr-1"
      />
      <a href="">Photos and Videos</a>
    </div>
    <div className="mb-4">
      <a href="">
        <img src="/image.jpg" alt="photo" className="w-20 h-20 mb-1 mr-1" />
        <img src="/image.jpg" alt="photo" className="w-20 h-20 mb-1 mr-1" />
        <img src="/image.jpg" alt="photo" className="w-20 h-20 mb-1 mr-1" />
        <img src="/image.jpg" alt="photo" className="w-20 h-20 mb-1 mr-1" />
      </a>
    </div>
  </div>
);

export default LeftProfile;
