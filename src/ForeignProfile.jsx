import React from 'react';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faEllipsisV } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

library.add(faEllipsisV);

const ForeignProfile = () => (
  <div className="bg-white shadow">
    <div className="container mx-auto flex items-center relative">
      <div className="w-1/4">
        <img
          src="/avatar.png"
          alt="avatar"
          className="rounded-full h-48 w-48 absolute pin-l pin-t -mt-24"
        />
      </div>
      <div className="w-1/2">
        <ul className="list-reset flex">
          <li className="text-center py-3 px-4 border-b-2 border-solid border-teal">
            <a href="" className="text-grey-darker hover:no-underline">
              <div className="text-sm font-bold tracking-tight mb-1">
                Tweets
              </div>
              <div className="text-lg text-bold tracking-tight text-teal">
                60
              </div>
            </a>
          </li>
          <li className="text-center py-3 px-4 border-b-2 border-solid hover:border-teal">
            <a href="" className="text-grey-darker hover:no-underline">
              <div className="text-sm font-bold tracking-tight mb-1">
                Following
              </div>
              <div className="text-lg text-bold tracking-tight hover:text-teal">
                4
              </div>
            </a>
          </li>
          <li className="text-center py-3 px-4 border-b-2 border-solid hover:border-teal">
            <a href="" className="text-grey-darker hover:no-underline">
              <div className="text-sm font-bold tracking-tight mb-1">
                Followers
              </div>
              <div className="text-lg text-bold tracking-tight hover:text-teal">
                3,180
              </div>
            </a>
          </li>
          <li className="text-center py-3 px-4 border-b-2 border-solid hover:border-teal">
            <a href="" className="text-grey-darker hover:no-underline">
              <div className="text-sm font-bold tracking-tight mb-1">Likes</div>
              <div className="text-lg text-bold tracking-tight hover:text-teal">
                9
              </div>
            </a>
          </li>
          <li className="text-center py-3 px-4 border-b-2 border-solid hover:border-teal">
            <a href="" className="text-grey-darker hover:no-underline">
              <div className="text-sm font-bold tracking-tight mb-1">
                Moments
              </div>
              <div className="text-lg text-bold tracking-tight hover:text-teal">
                1
              </div>
            </a>
          </li>
        </ul>
      </div>
      <div className="w-1/4 flex justify-end items-center">
        <div className="mr-6">
          <button className="btn-teal">Following</button>
        </div>
        <div>
          <a href="" className="text-grey-dark">
            <FontAwesomeIcon icon="ellipsis-v" size="lg" />
          </a>
        </div>
      </div>
    </div>
  </div>
);

export default ForeignProfile;
