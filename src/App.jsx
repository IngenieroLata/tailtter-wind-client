import React from 'react';
import { hot } from 'react-hot-loader';

import Header from './Header';
import ForeignProfile from './ForeignProfile';
import MainContent from './MainContent';
import LeftProfile from './LeftProfile';
import RightContent from './RightContent';
import './App.css';

const App = () => (
  <div>
    <Header />
    <div className="hero h-112 bg-cover" />
    <ForeignProfile />

    <div className="container mx-auto flex mt-3 text-sm leading-normal">
      <LeftProfile />
      <MainContent />
      <RightContent />
    </div>
  </div>
);

export default hot(module)(App);
